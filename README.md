# Wiki Cli

Command Line Client for quick lookups on Wikipedia.

## Features

- Lookup many languages
- Print preview formatted
- Display images

## TODOs

- Command line arguments parser

## Usage

```sh
$ pip install --user --upgrade wiki-cli
$ wiki <Word>
$ wiki 'Wikimedia Project'
```

[![asciicast](https://asciinema.org/a/KJK2Bk7uSV9l2k3VZVG6azpko.svg)](https://asciinema.org/a/KJK2Bk7uSV9l2k3VZVG6azpko)

## License

[MIT](LICENSE)
